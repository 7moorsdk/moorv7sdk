
Pod::Spec.new do |spec|


  spec.name         = "MoorV7SDK"
  spec.version      = "1.3.1"
  spec.summary      = "MoorV7SDK."


  spec.homepage     = "https://bitbucket.org/7moorsdk"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"



  spec.license      = "MIT"
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  spec.author             = { "VE66" => "942914231@qq.com" }

  # spec.platform     = :ios
  spec.platform     = :ios, "9.0"


  spec.source       = { :git => "https://bitbucket.org/7moorsdk/moorv7sdk.git", :tag => "#{spec.version}" }


  #spec.source_files  = "Classes", "Classes/**/*.{h,m}"
  #spec.exclude_files = "Classes/Exclude"

  # spec.public_header_files = "Classes/**/*.h"

  spec.vendored_frameworks  = "MoorV7SDK.framework"

  spec.requires_arc = true
  spec.pod_target_xcconfig = {'VALID_ARCHS'=>'armv7 x86_64 arm64'}


  spec.dependency 'SocketRocket', '~> 0.5.1'
  spec.dependency 'Qiniu', '~> 8.2.0'
  spec.dependency 'YYModel'
  spec.dependency 'FMDB', '~> 2.7.5'

end
